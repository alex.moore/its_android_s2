package blaaster.com.myapplication.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import blaaster.com.myapplication.R;
import blaaster.com.myapplication.model.User;

/**
 * Created by Alexander Mozhugov on 20.05.2018.
 */
public class UserAdapter extends RecyclerView.Adapter<UserAdapter.UserViewHolder> {
    private static final String TAG = UserAdapter.class.getSimpleName();
    private List<User> users;
    private OnItemClicked listener;
    LayoutInflater layoutInflater;

    public UserAdapter(List<User> users, Context context, OnItemClicked fragment) {
        this.users = users;
        layoutInflater = LayoutInflater.from(context);
        listener = fragment;
    }

    @NonNull
    @Override
    public UserViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = layoutInflater.inflate(R.layout.item_user, parent, false);
        view.setOnClickListener((View v) -> {
            int position = (Integer) v.findViewById(R.id.back).getTag();
            listener.onItemClicked(position);
        });
        return new UserViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull UserViewHolder holder, int position) {
        User user = users.get(position);
        holder.back.setTag(position);
        holder.firstName.setText(user.getFirstName());
        holder.lastName.setText(user.getLastName());
        holder.birthDate.setText("HARD CODE");
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public static class UserViewHolder extends RecyclerView.ViewHolder {
        public TextView firstName;
        public TextView lastName;
        public TextView birthDate;
        public View back;

        public UserViewHolder(View itemView) {
            super(itemView);
            back = itemView.findViewById(R.id.back);
            firstName = itemView.findViewById(R.id.tv_first_name);
            lastName = itemView.findViewById(R.id.tv_second_name);
            birthDate = itemView.findViewById(R.id.tv_birth_date);
        }
    }

    public interface OnItemClicked {
        void onItemClicked(int position);
    }
}
