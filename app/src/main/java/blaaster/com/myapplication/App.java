package blaaster.com.myapplication;

import android.app.Application;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import blaaster.com.myapplication.model.User;
import blaaster.com.myapplication.net.RestInterface;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Alexander Mozhugov on 20.05.2018.
 */
public class App extends Application {
    private static List<User> users;
    private RestInterface restInterface;

    @Override
    public void onCreate() {
        super.onCreate();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://jsonplaceholder.typicode.com/")
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();

        restInterface = retrofit.create(RestInterface.class);

        users = new ArrayList<>();
        for (int i = 0; i < 50; i++) {
            users.add(new User("Name " + i, i + " lastname", new Date(), 160 + i));
        }
    }

    public RestInterface getRestInterface() {
        return restInterface;
    }

    public static List<User> getUsers() {
        return users;
    }
}
