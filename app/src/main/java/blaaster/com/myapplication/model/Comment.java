package blaaster.com.myapplication.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Alexander Mozhugov on 03.06.2018.
 */
public class Comment {
    public int postId;
    public int id;
    public String name;

    @SerializedName("email")
    public String e_mail;

    public String body;


    @Override
    public String toString() {
        return "Comment{" +
                "postId=" + postId +
                ", id=" + id +
                ", name='" + name + '\'' +
                ", e_mail='" + e_mail + '\'' +
                ", body='" + body + '\'' +
                '}';
    }
}
