package blaaster.com.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import blaaster.com.myapplication.App;
import blaaster.com.myapplication.MainActivity;
import blaaster.com.myapplication.R;
import blaaster.com.myapplication.adapters.UserAdapter;

/**
 * Created by Alexander Mozhugov on 20.05.2018.
 */
public class FragmentList extends Fragment implements UserAdapter.OnItemClicked {
    public static final String KEY = "key";
    public static final String TAG = FragmentList.class.getSimpleName();

    private RecyclerView recyclerView;

    public FragmentList() {
        setRetainInstance(true);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.layout_fragment1, null);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView = view.findViewById(R.id.rv1);
        recyclerView.setAdapter(new UserAdapter(App.getUsers(), getContext(), this));
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d(TAG, "onActivityCreated: " + hashCode());
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: " + hashCode());
        Log.d(TAG, "Id : " + getArguments().getInt(KEY));
    }

    public static FragmentList newInstance(int id) {
        Bundle args = new Bundle();
        args.putInt(KEY, id);
        FragmentList fragment = new FragmentList();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onItemClicked(int position) {
        ((MainActivity) getActivity()).onItemClicked(position);
    }
}
