package blaaster.com.myapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import blaaster.com.myapplication.R;
import blaaster.com.myapplication.model.User;

/**
 * Created by Alexander Mozhugov on 20.05.2018.
 */
public class FragmentDetails extends Fragment {
    public static final String KEY = "key";
    private TextView textView;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.layout_fragment2, null);
        textView = view.findViewById(R.id.tv1);
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        getArguments().getString(KEY);
    }

    public void showDetails(User user) {
        textView.setText(user.getFirstName() + " " + user.getLastName());
    }

    public static FragmentDetails newInstance(String s) {

        Bundle args = new Bundle();
        args.putString(KEY, s);
        FragmentDetails fragment = new FragmentDetails();
        fragment.setArguments(args);
        return fragment;
    }
}
