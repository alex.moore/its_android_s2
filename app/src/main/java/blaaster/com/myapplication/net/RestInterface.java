package blaaster.com.myapplication.net;

import java.util.List;

import blaaster.com.myapplication.model.Comment;
import blaaster.com.myapplication.model.User;
import io.reactivex.Flowable;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;

/**
 * Created by Alexander Mozhugov on 03.06.2018.
 */
public interface RestInterface {
    @GET("comments/")
    Observable<List<Comment>> getComments();

    @GET("users/")
    Flowable<List<User>> getUsers();
}
