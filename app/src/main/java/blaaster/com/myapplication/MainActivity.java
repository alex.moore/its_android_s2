package blaaster.com.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.FrameLayout;

import java.util.List;

import blaaster.com.myapplication.adapters.UserAdapter;
import blaaster.com.myapplication.fragments.FragmentDetails;
import blaaster.com.myapplication.fragments.FragmentList;
import blaaster.com.myapplication.model.Comment;
import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity implements UserAdapter.OnItemClicked {
    private static final String TAG = MainActivity.class.getSimpleName();
    private boolean isDualPanel;
    private CompositeDisposable disposable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_with_fragments);
        Log.d(TAG, "onCreate");
        disposable = new CompositeDisposable();
    }

    private void createLeftFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame1, FragmentList.newInstance(100500));
        transaction.commit();
    }

    private void createRightFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        transaction.replace(R.id.frame2, FragmentDetails.newInstance("testValue"));
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
//        getComments();
        startActivity(new Intent(this, MapsActivity.class));
    }

    private void getComments() {
        Observable<List<Comment>> listObservable =
                ((App) getApplication()).getRestInterface().getComments();
        disposable.add(listObservable.subscribeOn(Schedulers.io()).
                observeOn(AndroidSchedulers.mainThread()).subscribe(comments -> {
            Log.d(TAG, "Success");
            Log.d(TAG, "size : " + comments.size());
            Log.d(TAG, comments.get(0).toString());
        }, throwable -> {
            Log.d(TAG, "Error : " + throwable.getMessage());
        }));
    }

    @Override
    protected void onPause() {
        super.onPause();

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        Log.d(TAG, "onRestoreInstanceState: ");
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG, "onStart: ");
        FragmentManager fm = getSupportFragmentManager();
        Fragment leftFragment = fm.findFragmentById(R.id.frame1);
        if (leftFragment == null) {
            createLeftFragment();
        }
        Fragment rightFragment = fm.findFragmentById(R.id.frame2);
        FrameLayout fl = findViewById(R.id.frame2);
        if (fl != null && rightFragment == null) {
            createRightFragment();
            isDualPanel = true;
        }
    }

    @Override
    public void onItemClicked(int position) {
        Log.d(TAG, "in activity : " + position);
        if (isDualPanel) {
            FragmentManager fm = getSupportFragmentManager();
            Fragment rightFragment = fm.findFragmentById(R.id.frame2);
            ((FragmentDetails) rightFragment).showDetails(App.getUsers().get(position));
        } else {
            Log.d(TAG, "onItemClicked: ");
            Intent intent =
                    new Intent(MainActivity.this, DetailsActivity.class);
            intent.putExtra(DetailsActivity.KEY_USER_ID, position);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        disposable.dispose();
    }
}
