package blaaster.com.myapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import blaaster.com.myapplication.fragments.FragmentDetails;
import blaaster.com.myapplication.model.User;

public class DetailsActivity extends AppCompatActivity {
    public static final String KEY_USER_ID = "key_user_id";
    private User user;
    private FragmentDetails fragmentDetails;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

    }

    @Override
    protected void onStart() {
        super.onStart();

        Intent intent = getIntent();
        int position = intent.getIntExtra(KEY_USER_ID, -1);
        user = App.getUsers().get(position);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        fragmentDetails = FragmentDetails.newInstance("hey");
        transaction.replace(R.id.container, fragmentDetails);
        transaction.commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fragmentDetails.showDetails(user);
    }
}
